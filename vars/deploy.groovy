
def call(def homeClient, def adminUrl, def usuario, def senha, nomeArtefato, def caminhoArtefato){
    try{

        sh """  java -Xms512M -Xmx512M -cp ${homeClient} weblogic.Deployer -adminurl ${adminUrl} -user ${usuario} -password ${senha} -name ${nomeArtefato} -stop -adminmode -graceful"""
        sleep(8)
        sh """ java -Xms512M -Xmx512M -cp ${homeClient} weblogic.Deployer -debug -stage -remote -verbose -upload -name ${nomeArtefato} -source ${caminhoArtefato}  -targets AdminServer -adminurl ${adminUrl} -user ${usuario} -password ${senha} -deploy -usenonexclusivelock """
        sleep(3)

    }catch(Exception e){
        sh 'Falha no deploy Erro ${e}'
    }
}
