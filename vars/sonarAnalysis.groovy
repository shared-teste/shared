
def call(def scannerHome, def IP_SONAR,  def LOGIN_SONAR){
    sh "${scannerHome}/bin/sonar-scanner -e -Dsonar.projectKey=DeployBack -Dsonar.host.url=${IP_SONAR} -Dsonar.login=${LOGIN_SONAR} -Dsonar.java.binaries=target -Dsonar.coverage.exclusions=**/.mvn/**,**/src/test/**,**/model/**,**Application.java"
}
